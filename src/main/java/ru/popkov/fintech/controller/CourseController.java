package ru.popkov.fintech.controller;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.popkov.fintech.model.Course;
import ru.popkov.fintech.model.Student;
import ru.popkov.fintech.service.CourseService;
import ru.popkov.fintech.service.StudentService;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Anton Popkov
 */
@RestController
@Validated
@AllArgsConstructor
@RequestMapping("/courses")
public class CourseController {

    private static final Logger logger = LoggerFactory.getLogger(CourseController.class);

    private final CourseService courseService;

    private StudentService studentService;

    @PostMapping(value = "/",
            consumes = APPLICATION_JSON_VALUE)
    public void addCourse(@Valid @RequestBody Course course) {
        courseService.saveCourse(course);
        logger.info("New course {} has been added!", course.getTitle());
    }

    @GetMapping(value = "/{id}",
            produces = APPLICATION_JSON_VALUE)
    public Course getCourse(@PathVariable Integer id) {
        return courseService.getCourseById(id);
    }

    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Integer id, @RequestBody Course course) {
        courseService.updateCourse(id, course);
    }

    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Integer id) {
        courseService.deleteCourse(id);
    }

    @PostMapping(value = "/{id}/add-student-to-course",
            consumes = APPLICATION_JSON_VALUE)
    public void addStudentToCourse(@PathVariable Integer id,
                                   @Valid @RequestBody Student student) {
        studentService.save(student);
    }
}
