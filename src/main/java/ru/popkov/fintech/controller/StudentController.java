package ru.popkov.fintech.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.popkov.fintech.model.Student;
import ru.popkov.fintech.service.StudentService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Anton Popkov
 */
@RestController
@Validated
@RequestMapping("/students")
public class StudentController {

    private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @PostMapping(value = "/",
            consumes = APPLICATION_JSON_VALUE)
    public void addStudent(@Valid @RequestBody Student student) {
        studentService.save(student);
        logger.info("New student {} has been added!", student.getName());
    }

    @GetMapping(value = "/",
            produces = APPLICATION_JSON_VALUE)
    public List<Student> getStudents() {
        return studentService.findAll();
    }

    @GetMapping(value = "/{id}",
            produces = APPLICATION_JSON_VALUE)
    public Student getStudent(@PathVariable UUID id) {
        return studentService.findStudent(id);
    }

    @PutMapping("/{id}")
    public void updateStudent(@PathVariable UUID id, @RequestBody Student student) {
        studentService.update(id, student);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable UUID id) {
        studentService.delete(id);
    }
}
