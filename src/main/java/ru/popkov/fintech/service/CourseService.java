package ru.popkov.fintech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.popkov.fintech.dao.CourseRepository;
import ru.popkov.fintech.model.Course;

import java.util.List;

/**
 * @author Anton Popkov
 */
@Service
public class CourseService {
    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public void saveCourse(Course course) {
        courseRepository.save(course);
    }

    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    public Course getCourseByTitle(String title) {
        return courseRepository.findCourseByTitle(title);
    }

    public Course getCourseById(int id) {
        return courseRepository.findCourseById(id);
    }

    public void updateCourse(int id, Course course) {
        courseRepository.update(id, course);
    }

    public void deleteCourse(int id) {
        courseRepository.delete(id);
    }

    public Course getCourseByMaxAvgAge() {
        return courseRepository.findCourseByMaxAvgAge();
    }

}
