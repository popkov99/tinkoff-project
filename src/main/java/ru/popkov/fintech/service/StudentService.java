package ru.popkov.fintech.service;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.popkov.fintech.dao.CourseRepository;
import ru.popkov.fintech.dao.StudentRepository;
import ru.popkov.fintech.model.Course;
import ru.popkov.fintech.model.Student;

import java.util.List;
import java.util.UUID;

/**
 * @author Anton Popkov
 */
@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public void save(Student student) {
        studentRepository.insert(student);
    }

    public Student findStudent(UUID uuid) {
        return studentRepository.findById(uuid).orElseThrow();
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public void delete(UUID uuid) {
        studentRepository.delete(uuid);
    }

    public void update(UUID uuid, Student student) {
        studentRepository.update(uuid, student);
    }
}
