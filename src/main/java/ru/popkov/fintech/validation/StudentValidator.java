package ru.popkov.fintech.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.popkov.fintech.model.Student;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @author Anton Popkov
 */
@Component
public class StudentValidator implements ConstraintValidator<StudentConstraint, Student> {

    private static final Logger logger = LoggerFactory.getLogger(StudentValidator.class);

    @Override
    public boolean isValid(Student student, ConstraintValidatorContext constraintValidatorContext) {
        if (Objects.isNull(student.getName()) || student.getName().isBlank()) {
            logger.error("Name must be not null");
            return false;
        }
        if (student.getAge() <= 0) {
            logger.error("Age must be greater than 0");
            return false;
        }
        if (student.getGrade() <= 0) {
            logger.error("Grade must be greater than 0");
            return false;
        }

        return true;
    }
}
