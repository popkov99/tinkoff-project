package ru.popkov.fintech.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Anton Popkov
 */
@Target({ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StudentValidator.class)
public @interface StudentConstraint {
    String message() default "Student is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
