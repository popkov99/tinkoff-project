package ru.popkov.fintech.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Anton Popkov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    private int id;
    private String title;
    private String description;
    private int requiredGrade;
}
