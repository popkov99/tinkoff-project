package ru.popkov.fintech.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.popkov.fintech.validation.StudentConstraint;

import java.util.List;
import java.util.UUID;

/**
 * @author Anton Popkov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StudentConstraint
public class Student {
    private UUID id;
    private String name;
    private Integer age;
    private Integer grade;
    private List<Course> courses;
}
