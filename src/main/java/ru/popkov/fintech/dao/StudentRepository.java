package ru.popkov.fintech.dao;
import org.apache.ibatis.annotations.Param;

import org.apache.ibatis.annotations.Mapper;
import ru.popkov.fintech.model.Student;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Anton Popkov
 */
@Mapper
public interface StudentRepository {
    List<Student> findAll();

    Optional<Student> findById(UUID id);

    void insert(Student student);

    void update(@Param("uuid") UUID uuid, Student student);

    void delete(UUID uuid);
}
