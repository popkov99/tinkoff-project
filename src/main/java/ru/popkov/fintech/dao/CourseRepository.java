package ru.popkov.fintech.dao;
import org.apache.ibatis.annotations.Param;

import org.apache.ibatis.annotations.Mapper;
import ru.popkov.fintech.model.Course;

import java.util.List;

/**
 * @author Anton Popkov
 */
@Mapper()
public interface CourseRepository {
    void save(Course course);

    List<Course> findAll();

    Course findCourseByTitle(String title);

    int updateIdAndDescription(@Param("id") int id, @Param("description") String description);

    Course findCourseById(int id);

    int update(int id, Course course);

    void delete(int id);

    Course findCourseByMaxAvgAge();


}
